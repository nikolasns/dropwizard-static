package com.gitlab.zloster.resources;

import com.gitlab.zloster.api.CustomObject;
import com.gitlab.zloster.api.CustomValidation;
import com.codahale.metrics.annotation.Timed;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Optional;
import javax.validation.Valid;

@Path("/validate")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ValidationResource {

    public ValidationResource() {
    }

    @GET
    @Timed
    public CustomObject sayHello(@QueryParam("name") Optional<String> name) {
        final String value = name.orElse("default value");
        return new CustomObject(1, value, null);
    }

    @POST
    public CustomObject validate(@Valid @CustomValidation CustomObject custom) {
    	//The CustomValidation always fails
        System.out.println("validate called: " + custom);
        return custom;
    }
}