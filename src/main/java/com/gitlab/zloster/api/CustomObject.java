package com.gitlab.zloster.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;

public class CustomObject {
    private long id;

    @Length(max = 3)
    private String content;
    private String otherContent;

    public CustomObject() {
        // Jackson deserialization
    }

    public CustomObject(long id, String content, String otherContent) {
        this.id = id;
        this.content = content;
        this.otherContent = otherContent;
    }

    @JsonProperty
    public long getId() {
        return id;
    }

    @JsonProperty
    public String getContent() {
        return content;
    }

    @JsonProperty
    public String getOtherContent() {
        return otherContent;
    }
}